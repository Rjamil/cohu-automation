package Cohu.CohuAutomation;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


import io.github.bonigarcia.wdm.WebDriverManager;

public class Base { 
	
	
	public static WebDriver driver;
	
	Properties prop;
	FileInputStream fis;
	
	
	String browser;
	String url;
	String username;
	String password;
	String profileName;
	String fromEmail;
	String toEmail;
	
	
	
	
	public WebDriver initializeDriver() throws IOException {
		
		
		prop = new Properties();
		fis = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\main\\resources\\data.properties");
		prop.load(fis); 
		
		browser= prop.getProperty("browser");;
		url = prop.getProperty("url");;
		username = prop.getProperty("username");
		password = prop.getProperty("password");
		profileName = prop.getProperty("profilename");
		fromEmail = prop.getProperty("senderemail");
		toEmail = prop.getProperty("receivingemail");


		

		if (browser.equals("chrome")) {

			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();

		} else if (browser.equals("firefox")) {

			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\geckodriver.exe");
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,System.getProperty("user.dir")+"\\FireFoxLogs.txt");
			driver = new FirefoxDriver();

		} else if (browser.equals("ie")) {

			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();

		}
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return driver; 

	}
	
	
//	public void getScreenshot(String result) throws IOException {
//		
//		File src =  ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(src, new File("C:\\" + result + " : screenshot.png"));
//	}
	
	public static void takeSnapShot(String result) throws Exception{

        //Convert web driver object to TakeScreenshot

        TakesScreenshot scrShot =((TakesScreenshot)driver);

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File(System.getProperty("user.dir") + "\\screenshots\\"+result+"screenshot.png");

                //Copy file at destination

                FileUtils.copyFile(SrcFile, DestFile);
                
                

    }
	
	
	public void emailWithReportAttachment() throws EmailException {


		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("rabibjamilqa@gmail.com", "zayn1515"));
		email.setSSLOnConnect(true);
		email.addTo("rabibjamil@gmail.com", "Rabib Jamil");
		email.setFrom("rabibjamilqa@gmail.com", "Rabib Jamil");
		email.setSubject("Test Report Attachement");
		email.setMsg("Extent Report of Test Run");
		
		File file = new File(System.getProperty("user.dir")+"\\Reports\\TestResport.html");
		// add the attachment
		email.attach(file);
		System.out.println("Report emailed!");

		// send the email
		email.send();

	}
	
	public void emailWithFailTestScreenshot(String result) throws EmailException {

		// Create the attachment
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath(System.getProperty("user.dir") + "\\screenshots\\" + result + "screenshot.png");
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		attachment.setDescription("Picture of John");
		attachment.setName("John");

		// Create the email message
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("rabibjamilqa@gmail.com", "zayn1515"));
		email.setSSLOnConnect(true);
		email.addTo("rabibjamil@gmail.com", "Rabib Jamil");
		email.setFrom("rabibjamilqa@gmail.com", "Me");
		email.setSubject("TEST FAILED");
		email.setMsg("Failed Test Screenshot");
		

		// add the attachment
		email.attach(attachment);

		// send the email
		email.send();

	}
			
		
	
	
	

}
