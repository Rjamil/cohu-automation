package Cohu.CohuAutomation;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Scanner;



import com.google.common.base.Splitter;

public class TestData {
	
	public Map<String, String> getTestData(String fileName, String testCaseId) throws URISyntaxException
    {
          String filePath = null;
          Map<String, String> map = null;

          try {
//                filePath = Paths.get(getClass().getClassLoader()
//                            .getResource("dataproviders\\CohuUI\\" + fileName).toURI());
        	  
        	  filePath = "C:\\Users\\Rabib.Jamil\\eclipse-workspace\\CohuAutomation\\dataproviders\\CohuUI\\testdata.csv";
                
                Scanner scanner = new Scanner(new File(filePath));
            
                while (scanner.hasNext()) {
                      map = Splitter.on(",").trimResults().withKeyValueSeparator("=").split(scanner.nextLine());
//                	map = scanner.nextLine();

                      if (map.get("testCaseId").trim().equals(testCaseId)) {
                            break;
                      }
                }

            scanner.close();
                
          } catch (Exception e) {
                System.out.println("Exception while reading the file " + e.getMessage());
                e.printStackTrace();
          }
          
          return map;
    }
	
	
	

}
