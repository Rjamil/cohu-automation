package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {

	WebDriver driver;
	
	By usernamefield = By.id("signin-username");
	By passwordfield = By.id("signin-password");
	By loginbutton = By.id("loginButton");
	
	
	
	
	
	public Login(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement usernameField () {
		
		return driver.findElement(usernamefield);
		
	}
	
	public WebElement passwordField () {
		
		return driver.findElement(passwordfield);
		
	}
	
	public WebElement loginButton () {
		
		return driver.findElement(loginbutton);
		
	}
	
}
