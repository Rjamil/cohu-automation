package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class MediaSetup {

	
WebDriver driver;
	
	By setupmedia = By.id("setupMedia");
	By mediawizard = By.xpath("//span[contains(text(),'Media Wizard')]");
//===================================================================================	
	By connectiont1 = By.id("connectionT1");
	By connection3g = By.id("connection3G");
	By connection4g = By.id("connection4G");
	By connectionnext = By.linkText("Next");
//===================================================================================
	By qualitymotionsmoothness = By.id("qualityMotion");
	By qualitynext = By.linkText("Next");
//===================================================================================
	By resolution720 = By.id("1280x720");
	By resolutionnext = By.linkText("Next");
//===================================================================================
	By profilename = By.id("profileNameWizard");
	By profilenext = By.linkText("Next");
//===================================================================================
	By finishnext = By.linkText("Finish");
//===================================================================================
	By successful = By.xpath("//span[contains(text(),'Setup Successful')]");
	By successfulokbutton = By.xpath("//button[@id='OK_Button']//span[@class='ui-button-text'][contains(text(),'OK')]");
//===================================================================================
	By humanreadmessage0 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Starting six step process of completing wizard setup.')]");
	By humanreadmessage1 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 1 of 6: Encoder4 was created.')]");
	By humanreadmessage2 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 2 of 6: Setting parameters on Encoder4 for you.')]");
	By humanreadmessage3 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 3 of 6: Creating your profile.')]");
	By humanreadmessage4 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 4 of 6:')]");
	By humanreadmessage5 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 5 of 6:')]");
	By humanreadmessage6 = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'Step 6 of 6:')]");
	By humanreadmessagefinalmessage = By.xpath("//div[@id='mediaWizardHumanReadMessage'][contains(text(),'The stream and profile were setup for you')]");
//====================================================================================
	By profilesettings = By.id("setupMediaProfile");
	By mediaprofilenameselect = By.id("mediaProfileNameSelect");
//====================================================================================
	By deleteicon = By.id("deleteIcon");
//====================================================================================
	By mediastream = By.id("setupMediaStream");
	By mediastreamselect = By.id("mediaCurrentStream");
//====================================================================================
	By humanreadmessage7 = By.xpath("//div[@id='mediaProfileHumanReadMessage'][contains(text(),'Deleting profile Cohu from the camera.')]");
	By humanreadmessage8 = By.xpath("//div[@id='mediaProfileHumanReadMessage'][contains(text(),'Cohu was deleted from the camera.')]");
	By humanreadmessage9 = By.xpath("//div[@id='mediaStreamHumanReadMessage'][contains(text(),'Deleting stream Encoder4 from the camera.')]");
	By humanreadmessage10 = By.xpath("//div[@id='mediaStreamHumanReadMessage'][contains(text(),'Encoder4 was deleted from the camera.')]");
	
	
	
	public MediaSetup(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	
	public WebElement setupMedia() {
		
		return driver.findElement(setupmedia);
		 
	}
	
	

	public WebElement selectMediaWizard() {
		
		return driver.findElement(mediawizard);
		
	}
	
	public WebElement connectionT1() {
		
		return driver.findElement(connectiont1);
		
	}
	
	public WebElement connection3G() {
		
		return driver.findElement(connection3g);
		
	}
	
	public WebElement connection4G() {
		
		return driver.findElement(connection4g);
		
	}
	
	public WebElement connectionNext() {
		
		return driver.findElement(connectionnext);
	}
	
//==================================================================================================	
	
	public WebElement qualityMotionSmoothness() {
		
		return driver.findElement(qualitymotionsmoothness); 
	}
	
	public WebElement qualityNext() {
		return driver.findElement(qualitynext);
	}
	
//==================================================================================================	
	
	public WebElement resolution720() {
		
		return driver.findElement(resolution720); 
	}
	
	public WebElement resolutionNext() {
		return driver.findElement(resolutionnext);
	}

//==================================================================================================
	
	public WebElement profileName() {
		
		return driver.findElement(profilename); 
	}
	
	public WebElement profileNext() {
		return driver.findElement(profilenext);
	}
	
//==================================================================================================
	
	public WebElement finish() {
		
		return driver.findElement(finishnext); 
	}
	
//==================================================================================================
	
	public WebElement stepSuccessfulMessage() {
		
		return driver.findElement(successful);
	}
	
	public WebElement stepSuccessfulOkButton() {
		
		return driver.findElement(successfulokbutton);
	}
	
//==================================================================================================
	
	public WebElement humanReadMessage0() {
		
		return driver.findElement(humanreadmessage0);
	}
	
	public WebElement humanReadMessage1() {
		
		return driver.findElement(humanreadmessage1);
	}
	
	public WebElement humanReadMessage2() {
		
		return driver.findElement(humanreadmessage2);
	}
	
	public WebElement humanReadMessage3() {
		
		return driver.findElement(humanreadmessage3);
	}
	
	public WebElement humanReadMessage4() {
		
		return driver.findElement(humanreadmessage4);
	}
	
	public WebElement humanReadMessage5() {
		
		return driver.findElement(humanreadmessage5);
	}
	
	public WebElement humanReadMessage6() {
		
		return driver.findElement(humanreadmessage6);
	}
	
	public WebElement humanReadMessageFinalMessage() {
		
		return driver.findElement(humanreadmessagefinalmessage);
	}
	
//==================================================================================================	
	
	public WebElement profileSettings() {
		
		return driver.findElement(profilesettings);
	}
	
	public WebElement mediaProfileNameSelect() {
		
		return driver.findElement(mediaprofilenameselect);
	}
	
//===================================================================================================
	
	public WebElement deleteIcon() {
		
		return driver.findElement(deleteicon);
	}
	
//===================================================================================================
	
	public WebElement streamSettings() {
		
		return driver.findElement(mediastream);
	}
	
	public WebElement streamNameSelect() {
		
		return driver.findElement(mediastreamselect);
	}
	
//====================================================================================================
	public WebElement humanReadMessage7() {
		
		return driver.findElement(humanreadmessage7);
	}
	
	public WebElement humanReadMessage8() {
		
		return driver.findElement(humanreadmessage8);
	}
	
	public WebElement humanReadMessage9() {
		
		return driver.findElement(humanreadmessage9);
	}
	
	public WebElement humanReadMessage10() {
		
		return driver.findElement(humanreadmessage10);
	}
	
	
}
