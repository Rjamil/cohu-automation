package Cohu.CohuAutomation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;

import pageObjects.Login;
import pageObjects.MediaSetup;







public class CohuAutomationDemo extends Base {
	Logger Log = LogManager.getLogger(CohuAutomationDemo.class);
 
	@BeforeMethod
	public void initialize() throws IOException {
		
		driver = initializeDriver();
		driver.get(url);
		Log.info("Browser is initialized!");
	}
	
	@Test(description = "Create Profile and Stream through Media Wizard with connection3G")
	public void connection3G() throws IOException, InterruptedException, URISyntaxException {
		
		TestData testdata = new TestData();
		String message0 = testdata.getTestData("testdata", "0").get("message0");
		String message1 = testdata.getTestData("testdata", "1").get("message1");
		String message2 = testdata.getTestData("testdata", "2").get("message2");
		String message3 = testdata.getTestData("testdata", "3").get("message3");
		String message4 = testdata.getTestData("testdata", "4").get("message4");
		String message5 = testdata.getTestData("testdata", "5").get("message5");
		String message6 = testdata.getTestData("testdata", "6").get("message6");
		String messagefinal = testdata.getTestData("testdata", "final").get("messagefinal");
		String message7 = testdata.getTestData("testdata", "7").get("message7");
		String message8 = testdata.getTestData("testdata", "8").get("message8");
		String message9 = testdata.getTestData("testdata", "9").get("message9");
		String message10 = testdata.getTestData("testdata", "10").get("message10");
		

		Login login = new Login(driver); 
		Thread.sleep(1000);
		login.usernameField().sendKeys(username);
		Thread.sleep(1000);
		login.passwordField().sendKeys(password);
		Thread.sleep(1000);
		login.loginButton().click();
		Thread.sleep(1000);
		MediaSetup mediaWizard = new MediaSetup(driver);
		mediaWizard.setupMedia().click();
		Thread.sleep(1000);
		mediaWizard.selectMediaWizard().click();
		Thread.sleep(1000);
		mediaWizard.connection3G().click();
		mediaWizard.connectionNext().click();
		Thread.sleep(1000);
		mediaWizard.qualityMotionSmoothness().click();
		mediaWizard.qualityNext().click();
		Thread.sleep(1000);
		mediaWizard.resolution720().click();
		mediaWizard.resolutionNext().click();
		Thread.sleep(1000);
		mediaWizard.profileName().sendKeys(profileName);
		mediaWizard.profileNext().click();
		Thread.sleep(1000);
		mediaWizard.finish().click();
		WebDriverWait wait = new WebDriverWait(driver, 30); 

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage0()));
		System.out.println(mediaWizard.humanReadMessage0().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage0().getText(), message0);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage1()));
		System.out.println(mediaWizard.humanReadMessage1().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage1().getText(), message1);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage2()));
		System.out.println(mediaWizard.humanReadMessage2().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage2().getText(), message2);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage3()));
		System.out.println(mediaWizard.humanReadMessage3().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage3().getText(), message3);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage4()));
		System.out.println(mediaWizard.humanReadMessage4().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage4().getText(), message4);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage5()));
		System.out.println(mediaWizard.humanReadMessage5().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage5().getText(), message5);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage6()));
		System.out.println(mediaWizard.humanReadMessage6().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage6().getText(), message6);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessageFinalMessage()));
		System.out.println(mediaWizard.humanReadMessageFinalMessage().getText());
		Assert.assertEquals(mediaWizard.humanReadMessageFinalMessage().getText(), messagefinal);

		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.stepSuccessfulMessage()));
		String message = mediaWizard.stepSuccessfulMessage().getText();
		System.out.println(message);
		Thread.sleep(1000);
		Assert.assertEquals(message, "Setup Successful");
		mediaWizard.stepSuccessfulOkButton().click();
		Thread.sleep(1000);

		mediaWizard.profileSettings().click();
		Select mediaName = new Select(mediaWizard.mediaProfileNameSelect());
		mediaName.selectByVisibleText(profileName);
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage7()));
		System.out.println(mediaWizard.humanReadMessage7().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage7().getText(), message7);
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage8()));
		System.out.println(mediaWizard.humanReadMessage8().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage8().getText(), message8);
		
		Thread.sleep(1000);
		mediaWizard.streamSettings().click();
		Select streamName = new Select(mediaWizard.streamNameSelect());
		streamName.selectByVisibleText("Encoder4");
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage9()));
		System.out.println(mediaWizard.humanReadMessage9().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage9().getText(), message9);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage10()));
		System.out.println(mediaWizard.humanReadMessage10().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage10().getText(), message10);
		
		Log.info("Connection3G Test run Successful");

	}

	@Test(description = "Create Profile and Stream through Media Wizard connection4G")
	public void connection4G() throws IOException, InterruptedException, URISyntaxException {

		TestData testdata = new TestData();

		String message0 = testdata.getTestData("testdata", "0").get("message0");
		String message1 = testdata.getTestData("testdata", "1").get("message1");
		String message2 = testdata.getTestData("testdata", "2").get("message2");
		String message3 = testdata.getTestData("testdata", "3").get("message3");
		String message4 = testdata.getTestData("testdata", "4").get("message4");
		String message5 = testdata.getTestData("testdata", "5").get("message5");
		String message6 = testdata.getTestData("testdata", "6").get("message6");
		String messagefinal = testdata.getTestData("testdata", "final").get("messagefinal");
		String message7 = testdata.getTestData("testdata", "7").get("message7");
		String message8 = testdata.getTestData("testdata", "8").get("message8");
		String message9 = testdata.getTestData("testdata", "9").get("message9");
		String message10 = testdata.getTestData("testdata", "10").get("message10");
		
		

		Login login = new Login(driver);
		Thread.sleep(1000);
		login.usernameField().sendKeys(username);
		Thread.sleep(1000);
		login.passwordField().sendKeys(password);
		Thread.sleep(1000);
		login.loginButton().click();
		Thread.sleep(1000);
		MediaSetup mediaWizard = new MediaSetup(driver);
		mediaWizard.setupMedia().click();
		Thread.sleep(1000);
		mediaWizard.selectMediaWizard().click();
		Thread.sleep(1000);
		mediaWizard.connection4G().click();
		mediaWizard.connectionNext().click();
		Thread.sleep(1000);
		mediaWizard.qualityMotionSmoothness().click();
		mediaWizard.qualityNext().click();
		Thread.sleep(1000);
		mediaWizard.resolution720().click();
		mediaWizard.resolutionNext().click();
		Thread.sleep(1000);
		mediaWizard.profileName().sendKeys(profileName);
		mediaWizard.profileNext().click();
		Thread.sleep(1000);
		mediaWizard.finish().click();
		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage0()));
		System.out.println(mediaWizard.humanReadMessage0().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage0().getText(), message0);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage1()));
		System.out.println(mediaWizard.humanReadMessage1().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage1().getText(), message1);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage2()));
		System.out.println(mediaWizard.humanReadMessage2().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage2().getText(), message2);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage3()));
		System.out.println(mediaWizard.humanReadMessage3().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage3().getText(), message3);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage4()));
		System.out.println(mediaWizard.humanReadMessage4().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage4().getText(), message4);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage5()));
		System.out.println(mediaWizard.humanReadMessage5().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage5().getText(), message5);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage6()));
		System.out.println(mediaWizard.humanReadMessage6().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage6().getText(), message6);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessageFinalMessage()));
		System.out.println(mediaWizard.humanReadMessageFinalMessage().getText());
		Assert.assertEquals(mediaWizard.humanReadMessageFinalMessage().getText(), messagefinal);

		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.stepSuccessfulMessage()));
		String message = mediaWizard.stepSuccessfulMessage().getText();
		System.out.println(message);
		Thread.sleep(1000);
		Assert.assertEquals(message, "Setup Successful");
		mediaWizard.stepSuccessfulOkButton().click();
		Thread.sleep(1000);

		mediaWizard.profileSettings().click();
		Select mediaName = new Select(mediaWizard.mediaProfileNameSelect());
		mediaName.selectByVisibleText(profileName);
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage7()));
		System.out.println(mediaWizard.humanReadMessage7().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage7().getText(), message7);
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage8()));
		System.out.println(mediaWizard.humanReadMessage8().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage8().getText(), message8);
		
		Thread.sleep(1000);
		mediaWizard.streamSettings().click();
		Select streamName = new Select(mediaWizard.streamNameSelect());
		streamName.selectByVisibleText("Encoder4");
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage9()));
		System.out.println(mediaWizard.humanReadMessage9().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage9().getText(), message9);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage10()));
		System.out.println(mediaWizard.humanReadMessage10().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage10().getText(), message10);
		
		Log.info("Connection4G Test run Successful");

	}

	@Test(description = "Create Profile and Stream through Media Wizard connectionT1")
	public void connectionT1() throws IOException, InterruptedException, URISyntaxException {

		TestData testdata = new TestData();

		String message0 = testdata.getTestData("testdata", "0").get("message0");
		String message1 = testdata.getTestData("testdata", "1").get("message1");
		String message2 = testdata.getTestData("testdata", "2").get("message2");
		String message3 = testdata.getTestData("testdata", "3").get("message3");
		String message4 = testdata.getTestData("testdata", "4").get("message4");
		String message5 = testdata.getTestData("testdata", "5").get("message5");
		String message6 = testdata.getTestData("testdata", "6").get("message6");
		String messagefinal = testdata.getTestData("testdata", "final").get("messagefinal");
		String message7 = testdata.getTestData("testdata", "7").get("message7");
		String message8 = testdata.getTestData("testdata", "8").get("message8");
		String message9 = testdata.getTestData("testdata", "9").get("message9");
		String message10 = testdata.getTestData("testdata", "10").get("message10");
		

		Login login = new Login(driver);
		Thread.sleep(1000);
		login.usernameField().sendKeys(username);
		Thread.sleep(1000);
		login.passwordField().sendKeys(password);
		Thread.sleep(1000);
		login.loginButton().click();
		Thread.sleep(1000);
		MediaSetup mediaWizard = new MediaSetup(driver);
		mediaWizard.setupMedia().click();
		Thread.sleep(1000);
		mediaWizard.selectMediaWizard().click();
		Thread.sleep(1000);
		mediaWizard.connectionT1().click();
		mediaWizard.connectionNext().click();
		Thread.sleep(1000);
		mediaWizard.qualityMotionSmoothness().click();
		mediaWizard.qualityNext().click();
		Thread.sleep(1000);
		mediaWizard.resolution720().click();
		mediaWizard.resolutionNext().click();
		Thread.sleep(1000);
		mediaWizard.profileName().sendKeys(profileName);
		mediaWizard.profileNext().click();
		Thread.sleep(1000);
		mediaWizard.finish().click();
		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage0()));
		System.out.println(mediaWizard.humanReadMessage0().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage0().getText(), message0);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage1()));
		System.out.println(mediaWizard.humanReadMessage1().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage1().getText(), message1);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage2()));
		System.out.println(mediaWizard.humanReadMessage2().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage2().getText(), message2);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage3()));
		System.out.println(mediaWizard.humanReadMessage3().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage3().getText(), message3);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage4()));
		System.out.println(mediaWizard.humanReadMessage4().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage4().getText(), message4);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage5()));
		System.out.println(mediaWizard.humanReadMessage5().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage5().getText(), message5);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage6()));
		System.out.println(mediaWizard.humanReadMessage6().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage6().getText(), message6);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessageFinalMessage()));
		System.out.println(mediaWizard.humanReadMessageFinalMessage().getText());
		Assert.assertEquals(mediaWizard.humanReadMessageFinalMessage().getText(), messagefinal);

		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.stepSuccessfulMessage()));
		String message = mediaWizard.stepSuccessfulMessage().getText();
		System.out.println(message);
		Thread.sleep(2000);
		Assert.assertEquals(message, "Setup Successful");
		mediaWizard.stepSuccessfulOkButton().click();
		Thread.sleep(1000);

		mediaWizard.profileSettings().click();
		Select mediaName = new Select(mediaWizard.mediaProfileNameSelect());
		mediaName.selectByVisibleText(profileName);
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage7()));
		System.out.println(mediaWizard.humanReadMessage7().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage7().getText(), message7);
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage8()));
		System.out.println(mediaWizard.humanReadMessage8().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage8().getText(), message8);
		
		Thread.sleep(1000);
		mediaWizard.streamSettings().click();
		Select streamName = new Select(mediaWizard.streamNameSelect());
		streamName.selectByVisibleText("Encoder4");
		mediaWizard.deleteIcon().click();
		
		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage9()));
		System.out.println(mediaWizard.humanReadMessage9().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage9().getText(), message9);

		wait.until(ExpectedConditions.visibilityOf(mediaWizard.humanReadMessage10()));
		System.out.println(mediaWizard.humanReadMessage10().getText());
		Assert.assertEquals(mediaWizard.humanReadMessage10().getText(), message10);
		
		Log.info("ConnectionT1 Test run Successful"); 
		

	}
	
//	@Test
//	public void run () throws IOException, InterruptedException, EmailException {
//		initializeDriver();
//		driver.get(url);
//		Thread.sleep(1000);
//		int a = 1;
//		Assert.assertEquals(a, 2);
//		
//	}

	
	@AfterMethod
	public void endTest() {
		driver.close();
		Log.info("Browser Closed!");
	}

}
