package resources;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.mail.EmailException;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;
 
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Cohu.CohuAutomation.Base;
import Cohu.CohuAutomation.listeners;
 
public class ExtentReporterNG implements IReporter {
    private ExtentReports extent;
 
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
    	
    	extent = new ExtentReports(System.getProperty("user.dir")+"\\Reports\\TestResport.html", true);
    	extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
    	
        for (ISuite suite : suites) {
            Map<String, ISuiteResult> result = suite.getResults();
 
            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();
                
                buildTestNodes(context.getPassedTests(), LogStatus.PASS);
                buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
                buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
            }
        }
        
        
        
        extent.flush();
        extent.close();
        
        System.out.println("Report Generated !!!");
        
        Base email =  new Base();
        try {
			email.emailWithReportAttachment();
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
    }
 
    private void buildTestNodes(IResultMap tests, LogStatus status) {
        ExtentTest test;
 
        
        
        if (tests.size() > 0) {
            for (ITestResult result : tests.getAllResults()) {
                test = extent.startTest(result.getMethod().getMethodName());
                

 
                for (String group : result.getMethod().getGroups())
                    test.assignCategory(group);
 
                String message = "Test " + status.toString().toLowerCase() + "ed";
 
                if (result.getThrowable() != null)
                    message = result.getThrowable().getMessage();
                	test.addScreenCapture("C:\\Users\\Rabib.Jamil\\eclipse-workspace\\CohuAutomation\\screenshots\\runscreenshot.png");
 
//                test.log(status, message);
                if(result.getStatus() == ITestResult.FAILURE) {
                	test.log(status,result.getName(), test.addScreenCapture(System.getProperty("user.dir") + "\\screenshots\\"+result.getName()+"screenshot.png"));
                }
                else {
                	test.log(status, message);
                }	
                
                test.getTest().setStartedTime(getTime(result.getStartMillis()));
                test.getTest().setEndedTime(getTime(result.getEndMillis()));
 
                extent.endTest(test);
            }
        }
    }
 
    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();        
    }
}